﻿Introduction to DBMS
====================
Over the past two decades, data has become a strategic asset for most organizations. Databases are used to store, manipulate, and retrieve data in nearly every type of organization, including business, health care, education, government, and libraries. Database technology is routinely used by individuals on personal computers and by employees using enterprise-wide distributed applications. Databases are also accessed by customers and other remote users through diverse technologies, such as automated teller machines, Web browsers, smartphones, and intelligent living and office environments. Most Web-based applications depend on a database foundation. 

Following this period of rapid growth, will the demand for databases and database technology level off? Very likely not! In the highly competitive environment of today, there is every indication that database technology will assume even greater importance. Managers seek to use knowledge derived from databases for competitive advantage. For example, detailed sales databases can be mined to determine customer buying patterns as a basis for advertising and marketing campaigns. Organizations embed procedures called alerts in databases to warn of unusual conditions, such as impending stock shortages or opportunities to sell additional products, and to trigger appropriate actions.

Although the future of databases is assured, much work remains to be done. Many organizations have a proliferation of incompatible databases that were
developed to meet immediate needs rather than based on a planned strategy or a well-managed evolution. Enormous amounts of data are trapped in older,
“legacy” systems, and the data are often of poor quality. New skills are required to design and manage data warehouses and other repositories of data and to fully leverage all the data that is being captured in the organization. 

There is a shortage of skills in areas such as 
• database analysis, 
• database design, 
• database application development, and
• business analytics. 

We address these and other important issues to equip you for the jobs of the future.

As information systems professionals, you must be prepared to analyze database requirements and design and implement databases within the context of information systems development.
You also must be prepared to consult with end users and show them how they can use databases (or data warehouses) to build decision models and systems for competitive advantage. And, the widespread use of databases attached to Web sites that return dynamic information to users of these sites requires that you understand not only how to link databases to the Web-based applications but also how to secure those databases so that their contents can be viewed but not compromised by outside users.


Database Terminology
===============
♦ Data – Raw facts from which the required information is derived. Data have little meaning unless they are grouped in a logical manner.
Historically, the term data referred to facts concerning objects and events that could be recorded and stored on computer media. For example, in a salesperson’s database, the data would include facts such as customer name, address, and telephone number.

This type of data is called structured data. The most important structured data types are numeric, character, and dates. Structured data are stored in tabular form (in tables, relations, arrays, spreadsheets, etc.) and are most commonly found in traditional databases and data warehouses.

• Information – Data that has been organized into a specific context such that it has value to its recipient. 

♦ Metadata – A lens through which data takes on specific meaning and yields information. Metadata are data that describe the properties or characteristics of end-user data and the context of that data. Metadata are data that describe the properties or characteristics of end-user data and the context of
that data. Some of the properties that are typically described include data names, definitions, length (or size), and allowable values. Metadata describing data context include the source of the data, where the data are stored, ownership, and usage.

♦ Record – A logically connected set of one or more fields that describes a person, place, event, or thing.

♦ File – A collection of related records that contain information of interest to the end user


Data Management
=============
Four actions are involved in data management (CRUD): 
1. Creation of data
2. Retrieval of data
3. Update or modification of data
4. Deletion of data

For that, data must be accessed and, for the ease of access, data must be organized.

The four actions, often abbreviated as CRUD, are the creation of data, the retrieval of data, the modification or updating of data, and the deletion of data. We use Database Management Systems (DBMS) tools like Data Definition Language (DDL) create data and Data Manipulation Language (DML) to manipulate data. In a database system, it is important that data retrieval be fast and efficient. This can be achieved through efficient query design and through optimized DBMS query execution. Updates to data must not leave the database in an inconsistent state; the DBMS must provide facilities to check constraints that follow from business rules. Care must be exercised when deleting data. The DBMS needs to allow for the possibility of preventing a deletion if there is dependent data in the database in order to preserve database consistency.  
  

Data Management
===============
Two approaches for accessing data exist:
Sequential access – requires that one pass through the first n-1 records in a data set to get to the nth  record
Direct access – allows for the retrieval of the nth record in a data set without having to look at the previous n-1 records

Important: A DBMS facilitates access of data without burdening the user with details of how the data is physically organized.

History of Data Management
===========================
1950s - File Systems
1960s - Hierarchical DBMS
1970s - Network DBMS and Relational DBMS
1980s - Object-Oriented DBMS
For more information about the Timeline of Database History, visit 
• A Timeline of Database History: http://quickbase.intuit.com/articles/timeline-of-database-history 
• A Brief History of Database Systems: http://www.comphist.org/computing_history/new_page_9.htm
• The Evolution Of Data Models And Approaches To Persistence In Database Systems: http://www.fing.edu.uy/inco/grupos/csi/esp/Cursos/cursos_act/2000/DAP_DisAvDB/documentacion/OO/Evol_DataModels.html


Limitations of File-Processing Systems
=======================================
The predecessor of a database system where records were stored in separate non-integrated files.
* Data are separated and isolated in a file processing environment
* Data integrity (data values are correct, consistent, complete and current) is often violated in isolated environments
* The structure of each file is embedded in the application programs
* File processing systems lack flexibility and are not amenable to structural changes in data
* Organizations find it hard to enforce standards for naming data in a file processing environment
* File processing are dependent upon a programmer who can either write or modify program code


So, What Is Desirable? 
======================
* Integrated data – ensures that data is correct, consistent, complete, and current
   ** Not data in isolation to be integrated by the application program/programmer. 
* Data Independence - the ability to modify a schema definition in one level without affecting a schema definition in a higher level. 
   ** Application program(s) immune to changes in storage  structure and access strategy
   ** Independent user views of data

History of Data Management
==========================
In the 1970s, the Standards Planning and Requirements Committee (SPARC) of the American National Standards Institute (ANSI) proposed what came to be known as the ANSI/SPARC three-schema architecture: conceptual, internal and external schema.

Three Perspectives of Metadata in a Database
============================================
* Conceptual Schema
   ** Core of the architecture
   ** Represents the global view of the structure of the entire database for a community of users
   ** Captures data specification (metadata)
   ** Describes all data items and relationships between data together with integrity constraints
   ** Separates data from the program (or views from the physical storage structure)
   ** Technology independent

* Internal Schema
   ** Describes the physical structure of the stored data (e.g., how the data is actually laid out on storage devices)
   ** Describes the mechanism used to implement access strategies (e.g., indexes, hashed addresses, etc.)
   ** Technology dependent
   ** Concerned with the efficiency of data storage and access mechanisms

* External Schema
   ** Represents different user views, each describing portions of the database
   ** Technology independent
   ** Views are generated exclusively by logical references

Physical and Logical Data Independence
======================================
* Physical Data Independence – The ability to modify the internal schema without causing the application program in the external schema to be rewritten.
   ** Definition: External views unaffected by changes to the internal structure
   ** How? Introduction of conceptual schema between the external views and the internal (physical) schema

* Logical Data Independence – The immunity of a user view from changes in the other user views. 
   ** Definition: External views unaffected by design changes (growth or restructuring) in conceptual schema
   ** How? External views generated exclusively through logical reference to elements in the conceptual schema
   ** Consequence: External views unaffected by changes to other external views

What is a Database System? 
===========================
* A collection of general-purpose software that facilitates the processes of defining, constructing, and manipulating a database for various applications.
* A self-describing collection of integrated records
   ** Self-describing – The structure of the database (metadata) is recorded within the database system – not in the application programs.
   ** Integrated – The responsibility for 'integrating' data items as needed is assumed by the DBMS instead of the programmer.

What is a Database?
===================
* Database – a collection of data that’s related to a particular topic or purpose. 


   ** Data consists of recorded facts that have implicit meaning.  
   ** Viewed through the lens of metadata, the meaning of recorded data becomes explicit.
   ** A database is self-describing in that the metadata is recorded within the database – not in applications programs.  

What is a Database Management System?
======================================
Database Management System (DBMS) is a collection of programs (software) that manage the database structure and that control shared access to the data in the database. 

DBMS  Functions
================
* Stores the definitions of data and their relationships (metadata) in a data dictionary; any changes made are automatically recorded in the data dictionary.
* Creates the complex structures required for data storage.
* Transforms entered data to conform to the data structures.
* Creates a security system and enforces security within that system.
* Creates complex structures that allow multiple-user access to the data.
* Performs backup and data recovery procedures to ensure data safety.
* Promotes and enforces integrity rules to eliminate data integrity problems.
* Provides access to the data via utility programs and from programming languages interfaces.
* Provides end-user access to data within a computer network environment.

Components of a DBMS
====================
The major components of a DBMS include one or more query languages; tools for generating reports; facilities for providing security, integrity, backup and recovery; a data manipulation language for accessing the database; and a data definition language used to define the structure of data. 

Types of Database Systems
==========================
Number of users: Single-user | Multiuser | Workgroup | Enterprise
Database site location: Centralized | Distributed
Database use: Online Transaction Processing) Database (OLTP) | Online Analytical Processing Database (OLAP)


Some Commercial DBMS
===================
* IBM & DB2: https://www.ibm.com/analytics/us/en/technology/db2/
* Oracle: https://www.oracle.com/database/index.html
* Microsoft Access & SQL Server: http://www.microsoft.com/en-us/download/details.aspx?id=50040 and https://www.microsoft.com/en-us/cloud-platform/sql-server
* MySQL: http://www.mysql.com/
 

Important Terms
===============
* Data Redundancy – exists when unnecessarily duplicated data are found in the database. For example, a customer's telephone number may be found in the customer file, in the sales agent file, and in the invoice file. 
* Data Anomaly – an error or inconsistency in the database. A poorly designed database runs the risk of introducing numerous anomalies. Update—an anomaly that occurs during the updating of a record. For example, updating a description column for a single part in an inventory database requires you to make a change to thousands of rows.

Three types of anomalies:
=========================
* Insertion – an anomaly that occurs during the insertion of a record. For example, the insertion of a new row causes a calculated total field stored in another table to report the wrong total.
* Deletion – an anomaly that occurs during the deletion of a record. For example, the deletion of a row in the database deletes more information than you wished to delete.
* Update – an anomaly that occurs during the updating of a record. For example, updating a description column for a single part in an inventory database requires you to make a change to thousands of rows.

The Relational Database Model
=============================
* Relational database model has a very successful track record and it is the dominant database model in the market.
* Was conceived by E. F. Codd in 1969, then a researcher at IBM. 
* The model is based on branches of mathematics called set theory and predicate logic. 
* The basic idea behind the relational model is that a database consists of a series of unordered tables (or relations) that can be manipulated using non-procedural operations that return tables
* Can be applied to both databases and database management systems (DBMS) themselves. 
* The relational fidelity of database programs can be compared using Codd’s 12 rules. The number of rules has been expanded to 300 for determining how DBMS products conform to the relational model


Steps in Database Design
========================
Step One: Determine the purpose of your database
Step Two: Determine the Entities (tables) you need
Step Three: Determine the attributes (fields) in each entity
Step Four: Create the Data Dictionary
Step Five: Determine the Relationships by creating the Entity-Relationship Diagram (ERD) 
Step Six: Transfer the ERD into a relational system (tables, fields, data types, etc.)

• Database design basics: https://support.office.com/en-US/Article/Database-design-basics-1eade2bf-e3a0-41b5-aee6-d2331f158280
• A Quick-Start Tutorial on Relational Database Design: http://www.ntu.edu.sg/home/ehchua/programming/sql/Relational_Database_Design.html 




Step 1: Determine the purpose of your database
===============================================
The first step in designing a Microsoft Access database is to determine the purpose of the database and how it's to be used. This tells you what information you want from the database. From that, you can determine what subjects you need to store facts about (the tables) and what facts you need to store about each subject (the fields in the tables).
Talk to the people who will use the database. Brainstorm about the questions you'd like the database to answer. Sketch out the reports you'd like it to produce. Gather the forms you currently use to record your data. You'll use all this information in the remaining steps of the design process.

When designing a database, you have to make decisions regarding how best to take some system in the real world and model it in a database. 
The benefits of a database that has been designed according to the relational model are numerous. Some of them are:
* Data entry, updates and deletions will be efficient.
* Data retrieval, summarization and reporting will also be efficient.
* Since the database follows a well-formulated model, it behaves predictably.
* Since much of the information is stored in the database rather than in the application, the database is somewhat self-documenting.
* Changes to the database schema are easy to make.

Start by writing down a list of questions the database should be able to answer. How many sales of our featured product did we make last month? Where do our best customers live? Who's the supplier for our best-selling product?
Next, gather all the forms and reports that contain information the database should be able to produce.

After gathering this information, you're ready for the next step.


Step 2: Determine the Entities (tables) you need, choose the major entities, or subjects
=================================================
Determining the tables in your database can be the trickiest step in the database design process. That's because the results you want from your database -- the reports you want to print, the forms you want to use, the questions you want answered -- don't necessarily provide clues about the structure of the tables that produce them. They tell you what you want to know, but not how to categorize the information into tables.

Look at the information you want to get out of your database and divide it into fundamental subjects you want to track, such as customers, employees, products you sell, services you provide, and so on. Each of these subjects is a candidate for a separate table.

Note One strategy for dividing information into tables is to look at individual facts and determine what each fact is actually about. For example, the customer address isn't about the sale; it's about the customer. This suggests that you need a separate table for customers. In the Products On Order report, the supplier's phone number isn't about the product in stock; it's about the supplier. This suggests that you need a separate table for suppliers.

* Entities (Tables) in the relational model are used to represent “things” in the real world. Each table should represent only one thing. These things (or entities) can be real-world objects or events
* The relational model dictates that each row in a table be unique. If you allow duplicate rows in a table, then there’s no way to uniquely address a given row via programming.
* You guarantee uniqueness for a table by designating a primary key (PK) – a column that contains unique values for a table
* Primary keys become essential when you start to create relationships that join together multiple tables in a database. A foreign key (FK) is a column in a table used to reference a primary key in another table. 
* Foreign Keys must have the same data type and field size as the primary key. 


Step 3: Determine the attributes (fields) in each entity
=====================================
To determine the fields in a table, decide what you need to know about the people, things, or events recorded in the table. You can think of fields as characteristics of the table. Each record (or row) in the table contains the same set of fields or characteristics. For example, an address field in a customer table contains customers' addresses. Each record in the table contains data about one customer, and the address field contains the address for that customer.

* Attribute - A fundamental characteristic of an entity type (i.e., the conceptual representation of a property).
* An attribute that has a discrete factual value and cannot be meaningfully subdivided is called an atomic or simple attribute
* A composite attribute can be meaningfully subdivided into smaller subparts with independent meaning


Tips for Determining Fields
===========================
Here are a few tips for determining your fields:
• Relate each field directly to the subject of the table. A field that describes the subject of a different table belongs in the other table. Later, when you define relationships between your tables, you'll see how you can combine the data from fields in multiple tables. For now, make sure that each field in a table directly describes the subject of the table. If you find yourself repeating the same information in several tables, it's a clue that you have unnecessary fields in some of the tables.

• Don't include derived or calculated data. In most cases, you don't want to store the result of calculations in tables. Instead, you can have Microsoft Access or other DBMS perform the calculations when you want to see the result. For example, the Products On Order report displays the subtotal of units on order for each category of product in the Northwind database. However, there's no Units On Order subtotal field in any Northwind table. Instead, the Products table includes a Units On Order field that stores the units on order for each individual product. Using that data, Microsoft Access calculates the subtotal each time you print the report. The subtotal itself doesn't need to be stored in a table.

• Include all the information you need. It's easy to overlook important information. Return to the information you gathered in the first step of the design process. Look at your paper forms and reports to make sure all the information you have required in the past is included in your Microsoft Access tables or can be derived from them. Think of the questions you will ask Microsoft Access. Can Microsoft Access find all the answers using the information in your tables?

• Store information in its smallest logical parts. You may be tempted to have a single field for full names, or for product names along with product descriptions. If you combine more than one kind of information in a field, it's difficult to retrieve individual facts later. Try to break down information into logical parts; for example, create separate fields for first and last name, or for product name, category, and description.



Step Four: Create the Data Dictionary
==========================
A DBMS component that stores metadata – data about data. Thus, the Data Dictionary contains the data definition as well as the characteristics and relationships. 

Specifying primary keys
=======================
Each entity or table should include a column or set of columns that uniquely identifies each row stored in the table. This is often a unique identification number, such as an employee ID number or a serial number. In database terminology, this information is called the primary key (PK) of the table. 

If you already have a unique identifier for a table, such as a product number that uniquely identifies each product in your catalog, you can use that identifier as the table’s primary key — but only if the values in this column will always be different for each record. You cannot have duplicate values in a primary key. For example, don’t use people’s names as a primary key, because names are not unique. You could easily have two people with the same name in the same table.

A primary key must always have a value. If a column's value can become unassigned or unknown (a missing value) at some point, it can't be used as a component in a primary key.

You should always choose a primary key whose value will not change. In a database that uses more than one table, a table’s primary key can be used as a reference in other tables. If the primary key changes, the change must also be applied everywhere the key is referenced. Using a primary key that will not change reduces the chance that the primary key might become out of sync with other tables that reference it.

Often, an arbitrary unique number is used as the primary key. For example, you might assign each order a unique order number. The order number's only purpose is to identify an order. Once assigned, it never changes.


Step Five: Determine the Relationships by creating the Entity-Relationship Diagram (ERD) 
===========================
Now that you've divided your information into tables, you need a way to tell Microsoft Access or other FBMS how to bring it back together again in meaningful ways. 

Microsoft Access is a relational database management system. That means you store related data in separate tables. Then you define relationships between the tables, and Microsoft Access uses the relationships to find associated information stored in your database.

* You define foreign keys in a database to model relationships in the real world. Relationships between real-world entities can be quite complex, involving numerous entities each having multiple relationships with each other. 
* We consider only relationships between pairs of entities (tables). 

So, to set up a relationship between two tables -- Table A and Table B -- you add one table's primary key to the other table, so that it appears in both tables. But how do you decide which table's primary key to use? To set up the relationship correctly, you must first determine the nature of the relationship. 

There are three types of relationships between tables:
• One-to-many relationships
• Many-to-many relationships
• One-to-one relationships

One-to-One Relationships
=========================
* Two tables are related in a one-to-one (1–1) relationship if, for every row in the first table, there is at most one row in the second table. True one-to-one relationships seldom occur in the real world. 
* one-to-one relationships may be necessary in a database when you have to split a table into two or more tables because of security or performance concerns 

One-to-Many Relationships
==========================
A one-to-many relationship is the most common type of relationship in a relational database. In a one-to-many relationship, a record in Table A can have more than one matching record in Table B, but a record in Table B has at most one matching record in Table A.
For example, the Suppliers and Products tables in the Northwind database have a one-to-many relationship.

* Two tables are related in a one-to-many (1–M) relationship if for every row in the first table, there can be zero, one, or many rows in the second table, but for every row in the second table there is exactly one row in the first table. 

Many-to-Many Relationships
===========================
In a many-to-many relationship, a record in Table A can have more than one matching record in Table B, and a record in Table B can have more than one matching record in Table A. This type of relationship requires changes in your database design before you can correctly specify the relationship to Microsoft Access.

To detect many-to-many relationships between your tables, it's important that you look at both directions of the relationship. For example, consider the relationship between orders and products in the Northwind Traders business. One order can include more than one product. So for each record in the Orders table, there can be many records in the Products table. But that's not the whole story. Each product can appear on many orders. So for each record in the Products table, there can be many records in the Orders table.

The subjects of the two tables -- orders and products -- have a many-to-many relationship. This presents a problem in database design. To understand the problem, imagine what would happen if you tried to set up the relationship between the two tables by adding the Product ID field to the Orders table. To have more than one product per order, you need more than one record in the Orders table per order. You'd be repeating order information over and over for each record that relates to a single order -- an inefficient design that could lead to inaccurate data. You run into the same problem if you put the Order ID field in the Products table -- you'd have more than one record in the Products table for each product. How do you solve this problem?
The answer is to create a third table (junction table) that breaks down the many-to-many relationship into two one-to-many relationships. You put the primary key from each of the two tables into the third table.

Each record in the Order Details table represents one line item on an order. The Order Details table's primary key consists of two fields -- the foreign keys from the Orders and Products tables. The Order ID field alone doesn't work as the primary key for this table, because one order can have many line items. The Order ID is repeated for each line item on an order, so the field doesn't contain unique values. The Product ID field alone doesn't work either, because one product can appear on many different orders. But together the two fields always produce a unique value for each record.

In the Northwind database, the Orders table and the Products table aren't related to each other directly. Instead, they are related indirectly through the Order Details table. The many-to-many relationship between orders and products is represented in the database using two one-to-many relationships:
• The Orders and Order Detail tables have a one-to-many relationship. Each order can have more than one line item, but each line item is connected to only one order.
• The Products and Order Detail tables have a one-to-many relationship. Each product can have many line items associated with it, but each line item refers to only one product.

* Two tables are related in a many-to-many (M–M) relationship when for every row in the first table, there can be many rows in the second table, and for every row in the second table, there can be many rows in the first table. 
* Many-to-many relationships can’t be directly modeled in relational database programs. 
* These types of relationships must be broken into multiple one-to-many relationships. 

Entity Relationship Diagram (ERD) Model
========================================
Chen Model is an Entity Relationship Diagram described by Peter Chen, characterized by its use of diamonds to depict relationships and rectangles to depict entities.
Crow’s Foot Model is a notation of the entity relationship diagram using a three-pronged symbol to represent the “many” sides of the relationship.

Cardinality assigns a specific value to connectivity. Cardinality expresses the specific number of allowed entity occurrences associated with a single occurrence of the related entity.

Relational Model represents a major breakthrough for users and designers because of its conceptual simplicity.
To create the database we transfer the Entity-Relationship Model into a Relational Model: an entity name becomes a table name, an attribute name becomes a field name, and a domain becomes a data type.

Having good relational database software is not enough to avoid the data redundancy. It is possible to create poor table structures even in a good database design. So how do we recognize a poor table structure, and how do we produce a good table? The answer to both questions is based on normalization. Normalization is a process for evaluating and correcting table structures to minimize data redundancies.

The Entity-Relationship Model (ERM) uses Entity-Relationship Diagrams (ERD) to represent the conceptual database as viewed by the end user.

A Model of the needed data is created based on the types of things about which the system needs to store information (data entities). For example, to process a new order the system needs to know about the customer, the items wanted, and the detail about the order. This model is called Entity-Relationship Diagram (ERD). ERD is used to represent conceptual data model.

An Entity-Relationship Database Model complements the relational data model concepts and it is represented in an Entity-Relationship Diagram based on entities, attributes, and relationships.


Refining the Design
===================
Once you have the tables, fields, and relationships you need, it's time to study the design and detect any flaws that might remain.
Create your tables, specify relationships between the tables, and enter a few records of data in each table. See if you can use the database to get the answers you want. Create rough drafts of your forms and reports and see if they show the data you expect. Look for unnecessary duplications of data and eliminate them.
As you try out your initial database, you will probably discover room for improvement. Here are a few things to check for:

• Did you forget any fields? Is there information that you need that isn't included? If so, does it belong in the existing tables? If it's information about something else, you may need to create another table.

• Did you choose a good primary key for each table? If you use it to search for specific records, is it easy to remember and type? Make sure that you won't need to enter a value in a primary key field that duplicates another value in the field.

• Are you repeatedly entering duplicate information in one of your tables? If so, you probably need to divide the table into two tables with a one-to-many relationship.

• Do you have tables with many fields, a limited number of records, and many empty fields in individual records? If so, think about redesigning the table so it has fewer fields and more records.

• Has each information item been broken into its smallest useful parts? If you need to report, sort, search, or calculate on an item of information, put that item in its own column.

• Does each column contain a fact about the table's subject? If a column does not contain information about the table's subject, it belongs in a different table.

• Are all relationships between tables represented, either by common fields or by a third table? One-to-one and one-to- many relationships require common columns. Many-to-many relationships require a third table.


Step Six: Transfer the ERD into a relational system (tables, fields, data types, etc.)
===================
Entity --> Table, Attribute --> Field (column) name
tblEmployees (EmpID, firstName, lastName,…) 

Step Seven: Choose a DBMS
========================
Oracle, SQL Server, Db2, Sybase, …












References:
==========
• Relational Database Management System for Windows. Document No. DB64084-0295. Microsoft Press
• Modern Database Management Twelfth Edition | Jeffrey A. Hoffer, V. Ramesh, Heikki Topi | ISBN: 978-1-292-10185-9 © Pearson Education Limited 2016

