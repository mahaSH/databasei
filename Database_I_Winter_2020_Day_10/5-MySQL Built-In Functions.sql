/* MySQL Built-In Functions */
/* Script Date: March 25, 2019 */
/* Developed by: Your Name */ 

/* switch to the current database KDFlix (DVDRentals) */
-- Syntax: use database_name
use KDFlix
;

select 5 + 2
;

select sqrt(16)
;

select abs(-5)
;

/* Comparison and Converting Functions 
greatest()  | least() | coalesce | isNull() | interval() | strComp() 
*/

/* Control Flow Functions 
if() | isNull() | nullIf() | case() | cast() 
*/

/* String Functions 
ASCII() | ORD() | char_length() | character_length() | length() | 
character() | collation() | concat() | concat_ws() | inStr() 
| locate() | lcase() | lower() | ucase() | upper() | left() | right() | 
repeat() | reverse() | substring() | 
*/

/* Numeric Functions
ceil() | ceiling() | floor() | cot() | mod() | PI() | pow() | power() |
round() | trancate() | sqrt() 
*/

/* Date and Time Functions
addDate() | date_add() | subDate() | date_sub() | extract() |
curDate() | current_date() | curTime() | current_time() | 
current_timestamp() | now() | date() | time() | year() | month() | 
day() | monthName() | dateDiff() | timeDiff() | hour() | minute() | second()
*/

/* Summarizing (Aggregate) Functions
sum() | count() | min() | max()
*/

/* Encryption Functions 
encode() | decode() | password() | md5() | sha() | sha1() | current_user() |
session_user() | system_user() | user() | connection_id() | database() | version() |
found_rows() | last_insert_id()
*/

/* what is the greatest value of 2, 3, and 10 */
select greatest(2, 3, 10) as 'The Greatest Value'
;

select greatest('a', 'b', 'aa', 'ab', 'bb') as 'The Greatest Value'
;

/* the coalesce() function returns the first value in the list that is not null */
select coalesce(null, '', 4, null) as 'Coalesce First Value'
;

/* isNull() returns a value of 1 if the expression is evaluated to isNull, otherwize it returns zero*/
select isNull(1 * null) as 'isNull Function'
;

/* strcmp comapres string values. It returns zero if the expression1 equals to expression2 
if expr1 < exor2 => -1 | if expr1 > expr2 => 1 | if expr1 = expr2 => 0
*/
select strcmp('big', 'bigger') as 'StrCmp Function'
;

/* if(condition, 'true_value', 'false_value') */
select if ( (10 > 20), 'True Value', 'False Value') as 'If Function'
;

select *
from DVDS
;

select DVDName, if(StatID = 'S1', 'Check Out', if(StatID = 'S2', 'Available', 'N/A'))
from DVDs
;

/* return dvd name, status, and rating. Display the result set 'Only One Disk' 
if the dvd has a single disk, otherwize display 'Check for extra disk!' */
select DVDName, StatID, ratingID, NumDisks,
if ((NumDisks > 1), 'Check for extra disk!', 'Only One Disk') as 'Extra Disks'
from DVDs
;

/* IfNull() evaluates the expression whether it is equal to null */
select ifNull( (10 * null), 'Incorrect Expression') as 'IsNull Function'
;

/* nullIf() returns null if expression 1 equals to exprression2, otherwise 
it returns expression1 */
select nullIf( (10 - 20), (20 - 10) ) as 'NullIf Function'
;

/* using string functions: 
left([expression], number) 
right([expression], number)
substring([expression], start, stop)
*/
select *
from DVDs
where DVDID = 1
;
-- return the string White, Christmas, and Chris
select DVDName, 
left(DVDName, 5) as 'Left Function',
right(DVDName, 9) as 'Right Function',
substring(DVDName, 7, 5) as 'Subsctring Function'
from DVDs
where DVDID = 1
;

/* return the employee full name */
select 
	E.EmpID as 'Employee ID',
    E.EmpFN as 'First Name',
    E.EmpMN as 'Middle Name',
    E.EmpLN as 'Last Name',
    -- (E.EmpFN + ' ' + E.EmpMN + ' ' + E.EmpLN) as 'Full Name'
    -- concat(E.EmpFN, ' ', ifNull(E.EmpMN, ''), ' ', E.EmpLN)
	concat_ws(' ', E.EmpFN, ifNull(E.EmpMN, ''), E.EmpLN)
from Employees as E
;

/* create a registration employee ID based on the first 2 characters of FirstName,
and 3 characters of lastName. Capitalize the Registration ID */
select 
	EmpID as 'Employee ID',
	upper(concat(left(EmpFN, 2), left(EmpLN, 3))) as 'Registration ID',
    concat_ws(' ', EmpFN, ifNull(EmpMN, ''), EmpLN) as 'Employee'
from Employees
;

/* return the dvd name, and the movie type desciption */
select D.DVDName, MT.MTypeDescrip
from DVDS as D
	inner join MovieTypes as MT
		on D.MTypeID = MT.MTypeID
;

/* return the dvd name, 
movie type desciption,
and Studio description
 */
select D.DVDName, MT.MTypeDescrip, S.StudDescrip
from DVDs as D
	inner join MovieTypes as MT
		on D.MTypeID = MT.MTypeId
	inner join Studios as S
		on S.StudID = D.StudID
;
        
/* Return the total number of DVDs */
select count(DVDID) as 'Number of DVDs'
from DVDs
;

/* return all dvds and sort them by dvd name in ascending order */
select *
from DVDs
order by DVDName asc
;

/* how many dvds with drama movie type description */
select count(DVDID), MTypeID
from DVDs
group by MTypeID
;
