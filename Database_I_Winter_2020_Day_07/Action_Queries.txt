﻿ACTION QUERIES
===============

An action query changes the data in its datasource or creates a new table. There are four types of action queries—append, delete, update, and make table—and except for the make table query, action queries make changes to the data in the tables on which they are based.

As their name suggests, action queries make changes to the data in the tables they are based on (except for make table queries, which create new tables). There are four types of action queries:

• Append query: Adds the records in the query’s result set to the end of an existing table.
• Delete query: Removes rows matching the criteria that you specify from one or more tables.
• Update query: Changes a set of records according to criteria that you specify.
• Make table query: Creates a new table and then creates records in it by copying records from an existing table.

Changes made by action queries cannot be easily undone, so if you later decide you didn’t want to make those changes, usually you will have to restore the data from a backup copy. For this reason, you should always make sure you have a current backup of the underlying data before running an action query.

To minimize the risk involved in running an action query, you can first preview the data that will be acted upon by viewing the action query in Datasheet view before running it. When you are ready to run an action query, double-click it in the Navigation Pane or click it and then press Enter.
Or, on the DESIGN tab, in the Results group, click Run.


Append Query
============
An append query adds a set of records from one or more source tables (or queries) to one or more destination tables. Typically, the source and destination tables reside in the same database, but they don’t have to. For example, suppose you acquire some new customers and a database that contains a table of information about those customers. To avoid entering that new data manually, you can append it to the appropriate table in your database.

Make Table Query
==================
A make table query is an action query that creates a new table and then creates records in it by copying records from an existing table. You use a make table query when you want to create a new table based on query criterion or criteria from an existing table. For example, you may work for a telecommunications provider and be provided with a table with thousands of cellphone service customers who live in various states. You may find it easier to work with a subset of customers from New York, New Jersey, and Connecticut who have had cellphone service for less than one year, since you need to contact and produce reports on only those individuals. 

A make table query can be advantageous in this example, especially if you want to ensure the data is stored in a separate table (i.e., you want to export the data as a table to a new database). If you run a make table query with no criterion or criteria, Access will make a duplicate of the table titled after the name you provide. This is helpful when you need to copy the data in a table or to archive data, especially before you run an append, update, or delete query in case it amends the table data in a way that you don’t want. 


Update Query
==============
An update query is an action query that changes a set of records according to specified criteria. Use an update query when you need to add, change, or delete the data in one or more existing records. You can think of update queries as a powerful form of the Find and Replace dialog box.

When making an update query, you enter a select criterion and an update criterion. Unlike the Find and Replace dialog box, update queries can accept multiple criteria. You can use them to update a large number of records in one pass and to change records in more than one table at one time. You can also update the data in one table with data from another—as long as the data types for the source and destination fields match or are compatible.


Delete Query
==============
A delete query is an action query that removes rows matching the criteria that you specify from one or more tables. A delete query is used to delete entire records from a table, along with the key value that makes a record unique. Typically, delete queries are used only when you need to change or remove large amounts of data quickly. 




Summarizing Table Data
======================
It is often necessary to count or summarize data in a table, column by column. Tables that contain columns of sales figures or other numbers need to be summed, averaged, or counted to be more useful. The Total row makes these tasks easy.

Much like the bottom row of a spreadsheet, the Total row is a feature in Access 2013 that makes it easy to sum, average, or count the values in a datasheet column. You can also find maximum or minimum values and use statistical functions such as standard deviation and variance. 

Aggregate functions are functions that calculate values across a range of data, such as in a column. You can use these functions in queries or in Visual Basic for Applications (VBA) code. Although you can still use those methods, the Total row saves your time by allowing you to choose one of these functions from a menu, applying it instantly. The Total row is a row inserted at the bottom of a table that provides a menu of functions for each column in the row.


Aggregate Function 	Description 					Data Types
==================	============					==========
Average 		Calculates the average value for a column 	Number, Decimal, Currency, Date/Time
Count 			Counts the number of items in a column 		All (except multivalued list)
Maximum 		Returns the item with the highest value 	Number, Decimal, Currency, Date/Time
Minimum 		Returns the item with the lowest value 		Number, Decimal, Currency, Date/Time
Standard Deviation 	Measures how widely values are dispersed from
			an average value
									Number, Decimal, Currency
Sum 			Adds items in a column 				Number, Decimal, Currency
Variance 		Measures the statistical variance of 
			all values in the column 			Number, Decimal, Currency


To Use Grouping
================
Why? Statistics often are used in combination with grouping; that is, statistics are calculated for groups of records. 















