Data Integrity Rules
====================
The relational model defines several integrity rules that, while not part of the definition of the Normal Forms are nonetheless a necessary part of any relational database. There are two types of integrity rules: general and database-specific.


General Integrity Rules
========================
The relational model specifies two general integrity rules. They are referred to as general rules, because they apply to all databases. They are: entity integrity and referential integrity.
The entity integrity rule is very simple. It says that primary keys cannot contain null (missing) data. The reason for this rule should be obvious. You can't uniquely identify or reference a row in a table, if the primary key of that table can be null.


Integrity type		Constraint type		Description
Domain				DEFAULT				Specifies default value for column
							CHECK					Specifies allowed value for column
							FOREIGN KEY		Specifies column in which values must exist
							NULL						Specifies whether NULL is permitted

Entity					PRIMARY KEY		Identifies each row uniquely
							UNIQUE					Prevents duplication of nonprimary keys

Referential			FOREIGN KEY		Defines columns whose value must match the primary key of this table
							CHECK					Specifies the allowed value for a column based on the contents of another column


PRIMARY KEY Constraints
========================
� PRIMARY KEY constraints identify one or more columns in a table that constitute a primary key 
� One PRIMARY KEY constraint is allowed per table 
� Value must be unique across constituent columns 
� Null values are not allowed in constituent columns

CREATE TABLE [HumanResources].[Department]( [DepartmentID] [smallint] IDENTITY(1,1) NOT NULL, [Name] [dbo].[Name], �
CONSTRAINT [pkC_Department_DepartmentID] PRIMARY KEY CLUSTERED ([DepartmentID] ASC) WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
)


DEFAULT Constraints
===================
�  DEFAULT constraints define a default column value when no value is provided 
� Only one DEFAULT constraint per column 
� Only applicable to INSERT statements 
� Some system supplied functions allowed

CREATE TABLE [Production].[Location]( ... [Availability] [decimal](8, 2) NOT NULL CONSTRAINT [df_Location_Availability] DEFAULT ((0.00)), [ModifiedDate] [datetime] NOT NULL CONSTRAINT [df_Location_ModifiedDate] DEFAULT (getdate())
)


CHECK Constraints
=================
� CHECK constraints restrict the values that can be entered into a column on INSERT or UPDATE 
� Can define multiple CHECK constraints per column 
� Can reference columns in the same table 
� Cannot contain subqueries

ALTER TABLE [HumanResources].[EmployeeDepartmentHistory] WITH CHECK ADD CONSTRAINT [CK_EmployeeDepartmentHistory_EndDate] CHECK (([EndDate]>=[StartDate] OR [EndDate] IS NULL))


UNIQUE Constraints
==================
� UNIQUE constraints ensure that every value in a column is unique 
� Only one null value allowed in a unique column 
� Can include one or more columns

CREATE TABLE [HumanResources].[Employee]( [EmployeeID] [int] IDENTITY(1,1) NOT NULL, [NationalIDNumber] [nvarchar](15) NOT NULL UNIQUE NONCLUSTERED, �
)


FOREIGN KEY Constraints
========================
� FOREIGN KEY constraints ensure referential integrity between columns in same or different tables 
� Must reference a PRIMARY KEY or UNIQUE constraint 
� User must have REFERENCES permission on referenced table

ALTER TABLE [Sales].[SalesOrderHeader] WITH CHECK
ADD CONSTRAINT [fk_SalesOrderHeader_Customer_CustomerID] FOREIGN KEY([CustomerID]) REFERENCES [Sales].[Customer] ([CustomerID])


