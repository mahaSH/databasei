

/* add froeign key constraint(s) to the DVDs table */

/* 1. Between DVDs and MovieTypes tables */

/* 2) Between DVDs and Studios tables */

/* 3) Between DVDs and Ratings tables */

/* 4) Between DVDs and Formats tables */

/* 5) Between DVDs and Status tables */


/* add foreign key constraint(s) to the table DVDParticipant */

/* 1) Between DVDParticipant and DVDs tables */
    
/* 2) Between DVDParticipant and Participants tables */

/* 3) Between DVDParticipant and Roles tables */


/* add foreign key constraint(s) to the table Orders */

/* 1) Between Orders and Customers tables */
        
/* 2) Between Orders and Employees tables */


/* add foreign key constraint(s) to the table Transactions */

/* 1) Between Transactions and Orders tables */
   
/* 2) Between Transactions and DVDs tables */



/*
Foreign keys in each table
dvdparticipant - 3
dvds - 5
orders - 2
transactions - 2
*/

/* set the DVD name to unique constraint */

/* check constraint
Syntax:
	alter table table_name
		add constraint ck_column_name_table_name check (condition)
*/

/* set a check constraint to the Transactions table 
on Date Due to be greater than or equal to Date Out */


