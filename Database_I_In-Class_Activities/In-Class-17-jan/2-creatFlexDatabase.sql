/*Purpose: Create a database tracks the inventory of DVDs, provides information about the DVDs,
records rental transactions, and stores the names of the store’s customers and employees.8/
/*Script date:january15,2020
Developed by :Maha M.Shawkat
*/
/*Partial Syntax 
create object-type object-Name
wHERE OBJECT-type:database
*/
/*Check if database exissts.if so,delete it and then recreate it
(Only in the Developement environment)
*/
create database if not exists MMFlix2020
;
-- Switch to MMFlix database
-- sYNTAX:USE DATABASE_NAME
use mmflix2020
;
/*Using the schema ciaus to create a database.Create a schema is the same syntax as create database in MYSQL, but not in microsoft SQL server
*/
create schema mydb1
;
/*Delete a database
Syntax: drop databse database-name
*/
drop database mydatabase
;
drop database mydb1
;