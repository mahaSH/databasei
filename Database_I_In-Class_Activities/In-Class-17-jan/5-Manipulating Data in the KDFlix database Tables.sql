/*
Purpose: Manipulating Data in the KDFlix database Tables.

Script Date: January 17, 2020
Developed By: Your Name
*/

/* switch to the current active database (KDFlix) */
use MMFlix2020
;

/* to answer the question about your data, follow these steps:
	1. read and understand the question
    2. select the object(s), table or view, from where the data comes
    3. select the column(s) from each object
    4. define criteria(s) - conditions, and run them one after another
*/

select 4+1;
select sqrt(16);
select abs(-5);
/* 1. list all dvd id, name, and year released */

select DVDId,DVDName,yearReleased
from dvds
;


/* 2. list all dvd id and names released in 2001 */
select DVDID,DVDName,yearReleased
from dvds
where yearReleased='2001'
;

/* 3. List all dvd id and names that have more than one disk */
select dvds.DVDID,dvds.DVDName,dvds.numdisk
from dvds
where NumDisk>1
;

-- changing the caption - Alias
-- re-write the previous script using aliases
/* 1. list all dvd id, name, and year released */

select d.DVDId as 'dvd Id',d.DVDName as 'dvd name',d.yearReleased as 'date released'
from dvds as d
;

/* using the aliases, change the column names in the result set of 
the table Employees. For example, EmpID - Employee ID, 
EmpFN - First Name, and so on. */
select empId as 'employee Id',empfn as'Employee first name',e.empmn as 'middle name',e.empln as 'last name'
from Employees as e
;

/* return the employee full name as a single string, 
using concatenation operator (+) */
select e.empfn
(e.empfn+""+e.empmn+""+e.empln) as 'Employee full name'
from employees as e
where e.empfn+e.empln
;

/* using the concat() function */

/* using coalesce function */


/* using the concat_ws() function */
select e.EmpID as 'employee ID',
concat_ws('',E.EmpFn, coalesce(e.empmn,''),e.empln)as 'Employee name'
from employees as e
;



/* change the DateIn to September 22,2020 for 
the transaction id number 1 */
update transactions
set dateIn='2020/01/22'
where transID=1
;

/* change the date due to date out plus 5 days 
for the transId number 2 */
update transactions
set datedue=dateOut+5
where transID=2
;

/* change the datein to january 2,2020
for the transId number 3 */
update transactions
set datein=date_add(DateIn,interval 1 week)
where transID=3
; 
-- remove one day from dateIn
update transactions
set datein=date_add(DateIn,interval -1 day)
where transID=3
; 

/* display the list of available DVDs for rent */
select *
from status
;
select *
from dvds
where statid='s2'
;
/* How many DVDs are available for rent? */
select count(DVDID) as 'available dvd for rent'
from dvds
where statid='s2'
;


/* 3. List all dvd id and names that have more than one disk. 
Sort disks in ascending order.
Flow control function
SYNTAX: if(condition, true_vale, false_value) */
select 
d.DVDID as 'dvd id',
d.dvdName as 'dvd name', 
d.statID as 'status',
d.numdisk as 'number of disks',
if((d.numdisk>1),'check for extra disk!','only one disk') as 'number of disks'
from dvds as d
order by d.numdisk desc
;
/* show the max rating of dvds .show only three raws*/
select 
d.rtingID as 'rating',
count(dvdid) as `number of dvds rated`
from dvds as d
-- Where clause
group by d.rtingid
order by `number of dvds rated`
-- group by d.rtingID
limit 3
;
