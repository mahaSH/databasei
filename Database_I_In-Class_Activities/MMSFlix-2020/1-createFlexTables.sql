/*Purpose: Create the flex Database tables
*/
/*Script date:january15,2020
Developed by :Maha M.Shawkat
*/
-- Switch to MMFlix database
-- sYNTAX:USE DATABASE_NAME
use mmflix2020
;

/*Create a table object
partial syntax
create table table_name
(
column_name data_type constraint(s),
column_name data_type constraint(s),
...
column_name data_type constraint(s)
)
;
Where constraint defines the business rules:null, not null,primry key<....alter*/
/*
Check if the table object exists.If so delete and then recreate.
Note:use these command only during the developement process and not in a production of database
*/
/*********Table No.1-Customers*********/
create table Customers
(
-- column_name data_type constraints
CustID smallint auto_increment not null primary key,
CustFN varchar(20) not null,
CustMN varchar(5) null,
CustLN varchar(20) not null
)
;

/*Modify the custMN and set the field size to varchar(10).
SYNTAX: alter object_type object_name
*/
alter table Customers
modify column CustMN varchar(10) null
;

/*Show the defination (structure) of table customers*/
show tables
;

show create table Customers
;

describe Customers
;

show columns from customers
;

show create database mmflix2020
;

-- Create lookup tables
/*********Table No.2-Roles*********/
create table Roles
(
RoleID varchar(4) not null primary key,
Roldescrip varchar(30) not null
)
;

/*********Table No.2-MovieTypes*********/

create table MovieTypes
(
MTypeID varchar(4) not null,
MTypeDescrip varchar(30) not null
)
;
-- Set the primary key for the movie type
alter table MovieTypes
add constraint pk_MovieTypes primary key (MTypeID)
;

/*********Table No.4-Studios*********/
create table studios
(
StudID varchar(4) not null primary key,
StudDescrip varchar(40) not null
)
;

/*********Table No.5-Ratings*********/
create table ratings
( ratingID varchar(2) not null primary key,
RatingDescript varchar(30) not null
)
;
-- Modify the column RatingID data type to varchar(4) in table Ratings
alter table Ratings
modify column RatingID varchar(4) not null
;
describe ratings
;

/*********Table No.5-Formats*********/
create table Formats
(
formId char(2) not null primary key,
FormDescript varchar(15) not null
)
;

/*********Table No.7-Status*********/
create table status
(
atatID char(2) not null primary key,
StatDescrip varchar(20) not null
)
;
/*********Table No.8-participant*********/
create table participant
(
PartID smallint auto_increment not null primary key,
PartFN varchar(20) not null ,
partMN varchar(20) null,
PartLN varchar(20) not null,
RollID varchar(4) null
)
;
/*remove the column rolId from the table participants*/
alter table participant
drop column RollID
;
/*********Table No.5-DVDs*********/
create table DVDs
(
DVDID smallint not null auto_increment primary key,
dvdName varchar(16) not null,
NumDisk tinyint not null,
yearReleased year not null,
MTypeID varchar(4) not null, -- forign key (MovieTypes)
StudId varchar(4) not null, -- forign key (studios)
rtingID varchar(4) not null, -- forign key (Ratings)
FormID char(2) not null, -- forign key (format)
statID char(2) not null -- Forign key(status)
)
;

-- change the data type of the column yearReleased to char(4)
alter table DVDs
modify column yearReleased  char(4) not null
;
 /*********Table No.10-dvdParticipant*********/
create table dvdParticipant
(
dvdID smallint not null, -- forign key (DVDs)
partID smallint not null, -- Forign key (participants)
RollID varchar(4) not null, -- forign key (Rolls)
constraint pk_DVDParticipant primary key clustered 
(
DVDID asc,
PartID asc,
RollID asc
)-- composite primary key

)
;
describe DVDParticipant
;
/*********Table No.11-Employees*********/
create table Employees
(
EmpID smallint auto_increment not null primary key,
EmpFN varchar(20) not null,
EmpMN varchar(20) null,
EmpLN varchar(20) not null
-- constraint pk_employeesID 
)
;
/*********Table No.12 Orders*********/
create table orders
(
OrderID int not null primary key,
CustID smallint not null -- forign key(customer )
)
; 
alter table orders
add column EmpID smallint not null -- foreign key (Employee)
;
/*********Table No.13 Transactions*********/
create table transactions
(
TransID int not null primary key,
OrderID int  not null,-- forign key (order)
DVDID smallint not null, -- forign ket(DVDs)
DateOut date not null,
DateDue date not null ,
DateIn date not null
)
;
/*******************************/
show tables

;
/*Return the information about the base table in my database*/
select *
from information_schema.tables
where table_type='base table'
and table_schema='MMFlix2020'
;
/*Return the information about the number of base table in my database*/
select count (table_name)
from information_schema.tables
where table_type='base table'
and table_schema='MMFlix2020'
;
/*Return the defination of table customers*/
select *
from information_schema.columns
where table_name='customers'
;
-- or
select column_name ,DATA_TYPE,IS_NULLABLE,COLUMN_DEFAULT
from information_schema.columns
where table_name='customers'
;
