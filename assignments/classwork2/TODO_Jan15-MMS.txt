/*********Table No.11-Employees*********/
create table Employees
(
EmpID smallint auto_increment not null primary key,
EmpFN varchar(20) not null,
EmpMN varchar(20) null,
EmpLN varchar(20) not null
)
;
/*********Table No.12 Orders*********/
create table orders
(
OrderID int not null primary key,
CustID smallint not null -- forign key(customer )
)
; 
/*********Table No.13 Transactions*********/
create table transactions
(
TransID int not null primary key,
OrderID int  not null,-- forign key (order)
DVDID smallint not null, -- forign ket(DVDs)
DateOut date not null,
DateDue date not null ,
DateIn date not null
)
;