/*
Purpose: Manipulating Data in the KDFlix database Tables.

Script Date: January 17, 2020
Developed By: Your Name
*/

/* switch to the current active database (KDFlix) */
use KDFlix2020
;

/* to answer the question about your data, follow these steps:
	1. read and understand the question
    2. select the object(s), table or view, from where the data comes
    3. select the column(s) from each object
    4. define criteria(s) - conditions, and run them one after another
*/


/* 1. list all dvd id, name, and year released */



/* 2. list all dvd id and names released in 2001 */

/* 3. List all dvd id and names that have more than one disk */


-- changing the caption - Alias
-- re-write the previous script using aliases


/* using the aliases, change the column names in the result set of 
the table Employees. For example, EmpID - Employee ID, 
EmpFN - First Name, and so on. */



/* return the employee full name as a single string, 
using concatenation operator (+) */


/* using the concat() function */

/* using coalesce function */


/* using the concat_ws() function */




/* change the DateIn to September 10, 2019 for 
the transaction id number 1 */


/* change the date due to date out plus 5 days 
for the transId number 2 */


/* display the list of available DVDs for rent */


/* How many DVDs are available for rent? */



/* 3. List all dvd id and names that have more than one disk. 
Sort disks in ascending order.
Flow control function
SYNTAX: if(condition, truve_vale, false_value) */


/* show the max rating of dvds */
